title: Introducción al análisis de redes
subtitle: Sociales
class: animation-fade
autor: Alejandro Zappala
link: http://cartografo.es/presentacion/analisis_redes
twitter: [@alayzappala](https://twitter.com/alayzappala)
description: Un recorrido por las herramientas, conceptos y procesos para el análisis de redes sociales
layout: true

.bottom-bar[
  {{twitter}}
]

---

![portada](./img/portada.png)

---

class: center, middle

# {{title}}
## [{{subtitle}}]

{{description}}



.footnote[
{{link}}

{{autor}}  
]


---
class: center, middle
# Introducción
---
class: center, middle
## [Análisis de redes (sociales)](https://es.wikipedia.org/wiki/An%C3%A1lisis_de_redes)
----

Estudio de las relaciones humanas mediante `Teoría de Grafos`


---

## [Análisis de redes (sociales)](https://es.wikipedia.org/wiki/An%C3%A1lisis_de_redes)
----
- Ojo con el [`sinécdoque`](https://es.wikipedia.org/wiki/Sin%C3%A9cdoque)
--

- Las redes sociales como objeto de estudio
- desde la sociología hasta la gestión del conocimiento en las empresas.
- asociación y medida de las `relaciones y flujos` entre las personas, grupos, organizaciones, computadoras, sitios web, así como cualquier otra entidad de procesamiento de información/conocimiento
- Los `nodos` en la red son personas y grupos mientras que los `enlaces` muestran relaciones o flujos entre los nodos
- herramientas tanto visuales como matemáticas para `el estudio de las relaciones humanas`


---
class: center, middle

## Teoría de grafos
----
###Un grafo es un conjunto de vértices o nodos unidos por aristas o arcos, que representan relaciones binarias



---

## [Teoría de grafos](https://es.wikipedia.org/wiki/Teor%C3%ADa_de_grafos)
----
![Grafos conexos](./img/Connexe_et_pas_connexe.jpg)

*Ejemplos de Grafos. Grafo conexo y no conexo*
---

## Teoría de grafos - Los siete puentes
----
![siete puentes de Königsberg | grabado del siglo 17](./img/siete_puentes.jpg)

*Los siete puentes de Königsberg | Grabado del siglo 17*
???
El problema de los siete puentes de Königsberg (Prusia oriental en el siglo XVIII -ciudad natal de Kant- y *actualmente, Kaliningrado*, en la óblast rusa de Kaliningrado) es un célebre problema matemático que fue resuelto por **Leonhard Euler** en **1736** y dio origen a la Teoría de los grafos.



## Bridges of Königsberg

A classic illustration of graph traversal is that of the Seven Bridges of Königsberg—a little puzzle dating back to the time of Immanuel Kant. Situated on a small strip of land sandwiched between Poland and Lithuania, Königsberg (now Kaliningrad) was founded in the 13th century and, until 1945, remained an important (although small) city in German culture, home to both Kant and Leonhard Euler, one of the greatest mathematicians of the 18th century.
The city was set on the Pregel River, and included two large islands that were connected to each other and to the mainland by seven bridges. The 16th century plan of the city (Figure 2-1) shows these bridges; only 5 of them survived the bombardments of World War II, and most of the buildings on the central island have been demolished (Figure 2-2).

An apocryphal story is that Immanuel Kant was well-known in the city for taking long walks so regular that people could set their clocks by him. He was endlessly frustrated by the fact that on his return home he had to pass over the same bridge as he took in the beginning of his walk (being a rather pedantic sort, I guess), so he posed the problem over dinner to his friend Leonhard Euler. In process, he inadvertently helped invent a new branch of mathematics.

Euler formulated this problem as a graph (Figure 2-3); the two banks of the river and the islands were represented as graph nodes and the bridges as edges. Thus, a path through the city becomes a path (or a traversal) through the graph. The question of the bridges, thus, is formulated as follows: is it possible to traverse a graph without repeating any edges (but possibly repeating nodes) and returning to the starting point.

Euler proved that this is indeed not possible—no path (i.e., a walk that does not repeat an edge) is possible through this graph.
Try it yourself on the graph—you will realize why it caused Kant enough frustration that he lobbied the mayor to build an eighth bridge.

---

## Teoría de grafos - Los siete puentes
----
![siete puentes de Königsberg | grabado del siglo 17](./img/siete_puentes_2.jpg)

???
Euler demostró que no era posible puesto que el número de líneas que inciden en cada punto no es par (condición necesaria para entrar y salir de cada punto, y para regresar al punto de partida, por caminos distintos en todo momento).

---

## Analisis de enlaces - Link Analysis
----

![Análisis de enlaces](./img/analisis_enlaces.jpg)

---
## Las redes sociales como plataformas digitales

- Antes de la explosión de las `redes sociales como plataformas digitales online` (Myspace, Facebook, Twitter...), el procedimiento habitual era hacer consultas en buscadores y blogs y tabular los resultados
- La explotación de los datos se ha `mercantilizado` y liberalizado
- Se han elaborado potentes sistemas de procesado de información para:

	- Marketing SEO
  - RRHH
	- Ciencia de datos
	- Periodismo de datos

---

## Introducción
----
1. Las redes sociales no son la web
--

2. Los motores de búsqueda tampoco son la web
--

3. La web no es Internet
--

4. Internet no es lo digital
--

5. Lo digital no es el mundo
--


`Hay más conexiones` a tener en cuenta que las de las plataformas como Twitter o Facebook, o incluso `que las meramente digitales`
---

## Análisis de las relaciones para comprender individuos y grupos
----

- Curiosamente, es `con las personas más intimas con quien se tiene más conexiones` digitales en número y en tiempo
--

- Las Redes Sociales no son el mundo real, pero sí un `reflejo`
--

- Nuestras relaciones, en conjunto `definen` quién somos y cómo actuamos
--

- Nuestro carácter, educación, carrera, grupo étnico, todos `interactúan` con nuestro patrón de relaciones y `deja marcas` indelebles en él
--

- Mediante la observación de estos patrones `podemos responder` muchas cuestiones acerca de nuestra `sociabilidad`


---

## Ejemplos
----

- Estudio de relaciones entre empresas a través de redes de inversión y mediante consejos de administración con miembros comunes
--

- Cómo la red social en torno a la *office* afecta en la productividad de una empresa, y cómo puede ser muy perjudicial ignorarlo
--

- A través de la financiación de una campaña electoral, cómo un único grupo de interés puede controlar los resultados
--

- Descubrir elementos en una red que solo consumen (*lurkers*) sin aportar e incentivarles a la participación
--

- Estudios de riesgo e impacto ante un jackeo de cuenta
--

- We shall look at the anatomy of fads and trends—which are often mediated by Twitter and Facebook, but are offline phenomena by nature.


???
We will also look beyond social media. We shall study the relationships between companies through investment networks and through shared boards of directors. We will look inside an organization and discover how the social network around the water cooler and lunchroom affects the company’s ability to perform—and how a company could shoot itself in the foot by ignoring this. We will look at campaign finance and discover how a single special-interest group can control the outcome of an entire election. We will explore the world of terrorists, revolutionaries, and radicals—from stories of the Khobar Towers bombing in 1998 and the 9/11 attacks, to the recent uprising in Egypt. We shall look at the anatomy of fads and trends—which are often mediated by Twitter and Facebook, but are offline phenomena by nature.

---

## Ejemplos
----

![Al-Qaeda style Network](./img/AlQaedastyleNetwork.jpg)

Simulación de red estilo *Al Qaeda*

---
## Relaciones binarias y relaciones con valor asociado
----
--

Relaciones `binarias`
- "Juan sigue a Pedro en twitter"

--

Relaciones `con valor asociado`
- "Juan ha retuiteado 4 veces a Pedro"
--


La `frecuencia de comunicación` es un valor muy útil para medir la `fuerza de la relación`
---
## Simetría y asimetría
----

--
Relaciones `asimétricas`
- Jefe <--> Empleado
- Profesor <--> Alumno

--

Relaciones `simétricas`
- Facebook, Linkedin: Requieren confirmación mutua que fuerza el sistema, aunque la relación humana real sea asimétrica
- Twitter, Blog: Mediante reciprocidad se simetriza la relación

???
En la vida real, las amistades y las relaciones romanticas son relaciones asimétricas

---
## Relaciones multimodales
----

Pueden existir relaciones entre `actores de diferente tipo`

- Empresas emplean personas
--

- Inversores compran activos en empresas
--

- Personas poseen información y recursos
--

- ...


---

## Flujo de trabajo
----
1. Decisión del objeto de estudio
2. Obtención de los datos
3. Limpieza y modelado
4. Visualización -como parte del proceso de análisis-
5. Análisis estadístico
6. Obtención de conclusiones
7. Toma de decisiones
8. Documentación del proceso
9. Redacción de las conclusiones y gráficos explicativos

---
class: center, middle
# 1. Decisión del objeto de estudio
----
## Estrategia

---
class: center, middle

# 2. Extracción de datos
----
## Scraping y Data Mining

---
## Data Mining
----
<iframe width="800" height="500" src="https://www.youtube.com/embed/ueAaIEr0PY4" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>

---

## Scraping
----
- Todas las páginas web están construidas con `código HTML`

- `SCRAPING` es la técnica para:
	- `extraer` el código de una página web;
	- procesar el contenido para `filtrar` lo que nos interese de la forma más automatizada posible;
	- `tabular` los datos obtenidos

---

## Scraping con interfaz de usuario
----
- [Data miner](https://data-miner.io/)

![Data-Miner.io](./img/data-miner.jpg)

---

## Data-Miner.io
----
- Herramienta online
- Extensión para Chrome
- Alta de usuario

---

## Data-Miner.io
----
- Práctica

---

## Tabula
----
- [Tabula: ](https://tabula.technology/)Herramienta para escrapear pdf

![](./img/tabula.jpg)

Researchers of all kinds use Tabula to turn PDF reports into Excel spreadsheets, CSVs, and JSON files for use in analysis and database applications

---

## Si quieres profundizar
----

[Presentación de Pablo Martín](https://speakerdeck.com/pr3ssh/extraccion-de-datos-para-el-desarrollo-con-herramientas-digitales)

[![Presentación de Pablo Martín](./img/Presentacion_PabloMartin.jpg)](https://speakerdeck.com/pr3ssh/extraccion-de-datos-para-el-desarrollo-con-herramientas-digitales)

---

## Scraping mediante programación
----

Los lenguajes de programación más populares para el *escrapeo* son **Python** y **R**

- simpleza de la sintaxis (alto nivel)
- multitud de *Bibliotecas* (*Libraries*)
- Herramientas para crear entornos de programación con interfaz de usuario, como [Jupyter](https://jupyter.org/) o [RStudio](https://www.rstudio.com/products/RStudio/)

![Jupyter](./img/Jupyter.png)

---

## Scraping de redes sociales digitales
-----

### A tener en cuenta:

- La dificultad de obtener datos de las principales redes sociales digitales consiste sobretodo, en que ese es precisamente `el modelo de negocio`
--

- Existen muchas herramientas *online* alrededor de esos datos, ofreciendo planes gratuitos con herramientas limitadas y planes de pago, ya que son `intermediarios`
--

- Si el objetivo de la investigación es no lucrativo o académico, habrá de tenerlo en cuenta
--

- Hay otros modelos de red social sin fines lucrativos, como [Mastodon](https://joinmastodon.org/). Aunque el valor de esos datos podría ser relativamente menor por su masa crítica, a veces no es cuestión de cantidad.

---

## Facebook
----
![Audiencia por paises](./img/facebook_audience.jpg)
Fuente: [Alexa(an Amazon company)](https://www.alexa.com/siteinfo/facebook.com)

- Puedes hacer consultas solamente `acerca de los contactos de un perfil propio`, o pagar.

---

## Twitter
----
![Audiencia por paises](./img/twitter_audience.jpg)
Fuente: [Alexa(an Amazon company)](https://www.alexa.com/siteinfo/twitter.com)
---

## Twitter
----

- [Gnip](https://developer.twitter.com/en/enterprise):
Servicio para la creación de aplicaciones que utilicen los datos de Twitter, bien estadísticamente, bien como clientes de datos de Twitter.
- `Alta de Usuario`: Para obtener credenciales con las que poder hacer consultas a sus servicios
- [Colección de API](http://support.gnip.com/sources/twitter/) con distintos propósitos
- [Planes *estándar*](https://developer.twitter.com/en/docs/basics/getting-started#get-started-app)
- Planes *Premium* a partir de más de 500 resultados por consulta

---

## Herramientas con IU para trabajar con datos de redes sociales
----
Existen [muchas aplicaciones](http://socialmediadata.org/social-media-research-toolkit/) `con interfaz de usuario` para la consulta y extracción de datos - que emplean el API antes mencionado, así como para el análisis y visualización


---
class: center, middle
# 3. Limpieza y modelado
----
## Creación y edición de tablas

---

## Limpieza y modelado
Los datos obtenidos, para poder ser objeto de análisis estadísticos, han de estar bien tabulados
--


Algunas reglas para nombrar columnas:

- No emplear palabras separadas
--

- No emplear signos de puntuación ni acentos
--

- utilizar guion bajo _ para organizar el tipo de dato
---

## Varios tipos de herramientas
----
- Con `herramientas ofimáticas` de edición de tablas (excell, libreoffice...)
Problemas de RAM con grandes volúmenes de datos
- Con herramientas para edición de tablas como [OpenRefine](http://openrefine.org/)
- Con interfaz para programar por partes, documentando el proceso: [Jupyter](https://jupyter.org/), [RStudio](https://www.rstudio.com/products/RStudio/))
- Mediante bloc de notas, creando programas ejecutables
- Con terminal (Bash, Python, R)


???
La principal característica para decantarnos por una herramienta es el volumen de los datos

---

class: center, middle
# 4. Análisis de los datos
----
## análisis estadísticos, de frecuencia de palabras... y análisis de redes
---


## Herramientas *online* de análisis y visualización de datos
----

**Histogramas, etc**

- [Tableau](https://www.tableau.com/). Tiene una [versión gratuita](https://public.tableau.com/s/resources)
- [Datawrapper](https://www.datawrapper.de/):Muy popular y de fácil uso para visualizaciones interactivas y responsivas. Tiene una parte geo.
- [Infogram](https://infogram.com/): Crea gráficos de línea interactivos, gráficos circulares y de barra. O diseña visuales más complejas, como nubes de palabras o infografías
- [Dipity](http://www.dipity.com/): es una herramienta online que te permite organizar datos por tiempo, es como una línea del tiempo más interactiva
- [Google Fusion Tables](https://support.google.com/fusiontables/answer/2571232?hl=en): Para grandes cantidades de datos. Es un proyecto experimental que terminará en diciembre de 2019
- [Gapminder](https://www.gapminder.org/downloads/): Visualización temporal de datos estadísticos

---

## Análisis de frecuencia de palabras - TF-IDF
----
- [TF-IDF (Term Frequency-Inverse Document Frequency)](https://es.wikipedia.org/wiki/Tf-idf)

- Técnica muy popular de minería de textos utilizada para:
	- Valorar la importancia de los términos que se repiten dentro de una colección de documentos.
	- Consulta de información para jerarquizar resultados
	- Extracción de palabras clave en páginas web
	- Categorizar documentos (asignar palabras clave)
	- Comparar documentos (comprobar plagios, etc)

---

## Si quieres profundizar en TF-IDF
----

- [The TF*IDF Algorithm Explained | Elephate](https://www.elephate.com/blog/what-is-tf-idf/)


---
class: center, middle
# 4. Visualización de datos
----
## Como parte del proceso de análisis

---

## Herramientas para visualización de redes
----
Existen herramientas de `exploración visual de los datos`, con las que se pueden detectar anomalías, relaciones...
- La herramienta más conocida es [Gephi](http://gephi.org), software de visualización de redes de instalación local.
- [Linkurious](https://linkurio.us/): Plataforma de visualización de redes
- [HYPHE](http://hyphe.medialab.sciences-po.fr/): Herramienta para crear 'web corpus'- *Escrapeo* y curación de datos
- [Sigma](http://sigmajs.org/): Biblioteca Javascript para representar diagramas de grafo


---
## [Truthy](https://truthy.indiana.edu/tools/) - Indiana University Network Science Institute
----


<iframe width="800" height="500" src="https://www.youtube.com/embed/SzHOXy7jO7I" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>

OSoMe (awe•some) is a joint project of the Indiana University Network Science Institute (IUNI) and the Center for Complex Networks and Systems Research (CNetS), aimed to study information diffusion in social media. Here we highlight findings, publications, and resources. Use our tools to explore how ideas spread through online social networks.

---
class: center, middle

# Grafos
----
## Estructura de los datos para grafos
---

## Estructura de los datos para Gephi
----

Necesitamos:
- una tabla de `nodos` (*nodes*), identificados con números enteros únicos
- una tabla de `aristas` (*edges*), que relacione pares de nodos

Para ello, hay diversas maneras. Generalizando:

- mediante software preparado para ello, como [table2net](https://github.com/medialab/table2net)
- mediante limpieza y modelado con herramientas de programación, como R o python.

---

class: center, middle
# 6. Obtención de conclusiones
----
Qué se puede extraer del análisis estadístico y de la visualización y con qué precisión.



---
class: center, middle

# 7. Toma de decisiones
---

## Toma de decisiones

- ¿Son suficientes datos? ¿Son fiables?
- Repetir el proceso con otras palabras clave y combinaciones
- Repetir el proceso con resultados en otro idioma

---
class: center, middle
# 8. Documentación del proceso
----
## No olvides documentar todo el proceso

---

## Documentación del proceso
----
- Prueba de la veracidad del proceso a terceras personas
- Por si has de interrumpir el proceso para atender otras cosas
- Para que pueda continuar el proceso otra persona

---

## Documentación del proceso
----
Un proyecto de análisis de datos ha de estar [perfectamente documentado](https://github.com/rdpeng/courses/blob/master/05_ReproducibleResearch/Checklist/index.md) para poder ser contrastado

- Utiliza un `sistema de control de versiones` durante todo el proceso ([git](https://git-scm.com/doc), por ejemplo)
- Documenta los procesos de análisis como `para poder repetirlos obteniendo los mismos resultados`
- Indica siempre `el origen de las fuentes de datos`, así como su `fecha`
- `Cuantos más detalles mejor`
  - metodología de captura de datos
  - herramientas utilizadas (versión)
  - formato de los datos obtenidos.
  - etc
- Utiliza `formatos abiertos` para compartir los resultados

---
class: center, middle
# 9. Redacción y publicación de resultados
----
## Contexto

---

## Publicación de resultados
Ten en cuenta el medio donde se va a representar
- Noticia puntual `VS` Reportaje de fondo
- Artículo académico `VS` gran público
- Entrada de Blog `VS` Web interactiva
- Impreso `VS` pantalla luminescente (donde cambian las reglas de color y de contraste)
- Si convive con otros formatos: Fotos, video, animaciones, histogramas, diagramas...
- Dispositivo final (móvil, tableta, ebook, PC, portatil...)
- Resoluciones y formatos

---

## Publicación de resultados
Es importante `medir los recursos` de que se dispone
- Datos dinámicos `VS` estáticos
- Proceso de filtrado y cálculo de datos cada vez que se consulte `VS` imagen estática que ahorre carga a los sistemas informáticos
- Servidor con base de datos `VS` documentos tabulados (JSON)
- Servicios de terceros
- Desarrollo web: HTML, CSS, Javascript, PHP...

Muchas veces un desarrollo interactivo es [`superfluo e innecesario`](https://www.goodreads.com/quotes/1185-those-are-my-principles-and-if-you-don-t-like-them-well), ya que una imagen estática bien diseñada sería suficiente, incluso más clara


???

Ojo con la disponibilidad y cambios de condiciones de los servicios a terceros a lo largo del tiempo

---
## Advertencia acerca de las *[3th parties](https://en.wikipedia.org/wiki/Third-party_software_component)*
----

![](./img/thereisnocloud-v2.jpg)

---
## Advertencia acerca de las *[3th parties](https://en.wikipedia.org/wiki/Third-party_software_component)*
----
- Cambios en las condiciones del servicio ([Google Maps](http://blog-idee.blogspot.com/2018/06/cambios-en-la-api-de-google-maps.html), [Twitter](https://hipertextual.com/2012/08/twitter-cambios-api))
- Fin del servicio ([Cloud Made](https://cloudmade.com/) )
- `Soberanía de los datos`
- `Distinta legislación` según el origen del servicio
- Valorar si es necesario

---
## Lecturas
----

- [A Gentle Introduction To Graph Theory | Vaidehi Joshi](https://medium.com/basecs/a-gentle-introduction-to-graph-theory-77969829ead8)
- [Graph Theory | Keijo Ruohonen](http://math.tut.fi/~ruohonen/GT_English.pdf)
- [Graph Theory Course | Sarada Herke](https://www.youtube.com/user/DrSaradaHerke/playlists?sort=dd&shelf_id=5&view=50)
- [El problema de los cuatro colores | Revista C2](https://www.revistac2.com/el-problema-de-los-cuatro-colores/)


---

class: center, middle

##Gracias por vuestra atención
![contain image](./img/cc-by-nc-sa.png)
- Slideshow created using [remark](http://github.com/gnab/remark)
.footnote[{{autor}}]
